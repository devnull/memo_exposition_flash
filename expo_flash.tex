\documentclass[a4paper,12pt]{article}
\usepackage{fontspec}
\usepackage[french]{babel}
\usepackage{biolinum}
%\usepackage{xcolor}
\usepackage{xfrac}
\usepackage{amsmath}
\usepackage{booktabs}
\usepackage[squaren,Gray]{SIunits}
\usepackage{adjustbox}
\usepackage[table]{xcolor} % loads also "colortbl"
\usepackage{enumitem}
\usepackage{pifont}
\usepackage{amsthm}
\usepackage{amssymb}
%\usepackage{graphicx}
%\usepackage[labelformat=empty]{caption}

\setcounter{secnumdepth}{0}
\setlist[description]{leftmargin=1cm,labelindent=1cm}

\title{Mémo expo manuelle en	photographie}
\pagenumbering{gobble}

\begin{document}
    \newtheorem*{carreinv}{Loi en carré inverse}
	\vspace*{-2,2cm}
	\section{Exposition au flash}\label{exposition-au-flash}
	\subsection{Trop de pertes ?}\label{perte}
	La lumière du flash, comme toute quantité physique, se perd rapidement
	en fonction de la distance selon la loi en carré inverse :
	
	\begin{carreinv}
	   « Une quantité physique est inversement proportionnelle au carré de la distance de l'origine de cette quantité physique. »	
	\end{carreinv}
	
	Dans le cas de l'éclairage en photographie, quelle que soit la source de
	lumière (flash ou continue) utilisée, l'éclairement est inversement
~	proportionnel au carré de la distance source d'éclairage~$\Leftrightarrow$~sujet.
	En doublant cette distance, le sujet sera 4 fois moins éclairé, soit une perte de 75\%
	par rapport à la quatité de lumière reçue à la distance initiale.
	
	$$\acute{E}clairement \propto \frac{1}{D²}$$

	
	Cette règle est applicable quel que soit l'ordre de grandeur de
	distance considéré, aussi bien en mètres (grands sujets) qu'en centimètres
	(proxiphotographie et macrophotographie).
	
	\subsection{Le nombre guide et distance utile du flash}\label{le-nombre-guide-et-distance-utile-du-flash}
	Le nombre-guide NG permet de calculer la bonne distance en mètres (ou en
	pieds) d'éclairage au flash, en fonction de l'ouverture. Le NG, exprimé
	à ISO 100, est habituellement donné dans la documentation des flashes
	soit uniquement à pleine puissance \sfrac{1}{1} pour une focale de 50 mm sur un
	capteur $\unit{24\times36}{\milli\meter}$, soit sous forme de tableau en fonction de deux
	paramètres :
	
	\begin{itemize}[label=\ding{43}]
		\item
		  La puissance réglable de \sfrac{1}{n} jusqu'à 1. Typiquement par palier de \sfrac{1}{3},
		  à partir de \sfrac{1}{128}\ieme{}, sur les flashes cobra.
		\item
		  La $focale_{t\hat{e}te\_flash}$ pour la plage couverte par le flash, en m et en ft.
	\end{itemize}
	\vspace{0.2cm}
	Défini par :
	$$NG = D\times{}f$$
	
	\begin{list}{}
		\item $NG$ = Nombre-guide pour un puissance et une $focale_{t\hat{e}te\_flash}$ données.
		\item $D$ = Distance flash~$\Leftrightarrow$~sujet en mètres.
		\item $f$ = Ouverture du diaphragme, dit \og{}nombre f\fg{}.
	\end{list}
	\vspace{0.2cm}
	\noindent{}Distance en mètres :
	$$D = \frac{NG}{f}$$
	
	\noindent{}Pour un flash au nombre-guide en mètres de 33, à f/8 et 100 ISO
	$$D = 33\div8 = \unit{4,125}{\meter} \approx \textcolor{red}{\unit{4,1}{\meter}}$$
	
	\subsection{Conversion du Nombre Guide}\label{conversion-du-nombre-guide}
	\paragraph{Pour tout $ISO \neq 100$\\}\label{pour-tout-iso-neq-100}
	$$NG_{(N\_ISO)} = NG_{100}\times\sqrt{\frac{N\_ISO_{(iso)}}{100_{(iso)}}}$$

	\paragraph{Exemple pour 200 ISO :\\}\label{exemple-pour-iso-200}
	
	\begin{align}
		NG_{200} &= NG_{100}\times\sqrt{\frac{200}{100}} \nonumber \\
				 &= 33\times\sqrt{2} \approx 46,669 \approx \textcolor{red}{46,7} \nonumber 
	\end{align}
	
	\paragraph{Distance pour F/8 à 200 ISO :\\}\label{distance-pour-f8-200-iso}
	
	$$D = 46,7\div8 = \unit{5,8375}{\meter} \approx \textcolor{red}{\unit{5,8}{\meter}}$$	
	\paragraph{NG équivalent pour 2 flashes de NG différents\\}\label{ng-uxe9quivalent-pour-flashes-de-ng-diffuxe9rents}
	
	$$NG_{fl1+fl2} = \sqrt{NG_{fl1}²+NG_{fl2}²}$$
	
	\vspace{0.2cm}
	\hspace{-0.21cm}Pour $NG_{fl1} = 33$ et $NG_{fl2} = 36$
	\begin{align}
		NG_{33+36} &= \sqrt{33²+36²} \nonumber \\
				   &= \sqrt{1089+1296} \nonumber \\
				   &= \sqrt{2385} \nonumber \\
				   &= 48,8364617883 \approx \textcolor{red}{48,83} \nonumber
	\end{align}
	
	\paragraph{Distance pour F/8 avec 2 flashes de NG :\\}\label{distance-pour-f8-flashes}
	
	Pour $NG_{fl1} = 33$ et $NG_{fl2} = 36$
	$$D = 48,83\div8 = \unit{6,10375}{\meter} \approx \textcolor{red}{\unit{6,1}{\meter}}$$
	
	\vspace{0.5cm}
	Note :\\
	\indent{}Le nombre-guide décroît si un filtre de couleur ou un diffuseur est utilisé.\\\indent{}En flash manuel, l'ajout du filtre CTO (éclairage incandescent) fourni avec le~430EX~III nécessite compensation d'environ +1~stop.
	
	\vspace*{-2cm}
	\section{Bonus}\label{bonus}
	\subsection{Durée T0.1 de l'éclair d'un flash}\label{duruxe9e-t0.1-de-luxe9clair-dun-flash}
	La durée T0.1 d'un éclair de flash (émission de 90\%, $\approx3\times{}T0.5$) renseigne sur sa capacité à figer un mouvement rapide, contrairement à la durée T0.5 (50\% émise/dissipée) généralement communiquée par les fabricants.

	\vspace{0.2cm}
	\hspace{2cm}\textcolor{red}{Tableau des durées T0.1 du flash 430EXIII-RT}
	\begin{table}[h!]
		\rowcolors{2}{gray!25}{white}
		\begin{tabular}{*{9}c|}
			\rowcolor{gray!50}
			\bottomrule
			\multicolumn{1}{|r|}{\textbf{Puissance}} & 1 & \sfrac{1}{2} & \sfrac{1}{4} & \sfrac{1}{8} & \sfrac{1}{16} & \sfrac{1}{32} & \sfrac{1}{64} & \sfrac{1}{128}\\
			\hline
			\multicolumn{1}{|r|}{\textit{secondes}} & \sfrac{1}{464} & \sfrac{1}{1370} & \sfrac{1}{3173} & \sfrac{1}{5130} & \sfrac{1}{7250} & \sfrac{1}{10136} & \sfrac{1}{12967} & \sfrac{1}{16500}\\
			\multicolumn{1}{|r|}{\textit{$\micro{}secondes$}} & 2153,0 & 730,0 & 315,0 & 195,0 & 138,0 & 99,0 & 77,0 & 61,0\\
			\toprule
		\end{tabular}
	\end{table}
	\vspace{-0.8cm}
	\subsection{Ouverture}\label{ouverture}
	\paragraph{Introduction\\[12pt]}\label{introduction}
	
	\begin{minipage}{0.2\textwidth}
		\vspace{-0.7cm}
		\begin{align}
			N &= F/d \nonumber \\
			d &= F/N \nonumber 
		\end{align}	
	\end{minipage}
	\begin{minipage}{0.75\textwidth}
		\vspace{-0.88cm}
		\begin{list}{}
			\item $d$ = Diamètre de la pupille d'entrée.
			\item $F$ = distance focale.
			\item $N$ = Ouverture du diaphragme, dit \og{}nombre f\fg{}.
		\end{list}	
	\end{minipage}

	\paragraph{Notation APEX (Additive system of Photography EXposure)\\[12pt]}\label{notation-apex-additive-system-of-photography-exposure}
    
	L'ouverture est représentée sur une échelle logarithmique par l'indice~d'ouverture~$A_v$, également notée AV (Aperture Value), définie par :
	
	\begin{align}
		A_v &= 2log_2(N) \nonumber \\
		N² &= 2^{A_v} \nonumber 
	\end{align}

	\hspace{2cm}\textcolor{red}{Tableau des ouvertures par palier de 1/3 EV}
	\begin{table}[h!]
		\rowcolors{1}{gray!25}{white}
		\begin{adjustbox}{width={\textwidth},totalheight={\textheight},keepaspectratio}%
		\begin{tabular}{*{10}c|}
			%\rowcolor{gray!50}
			\bottomrule
			\multicolumn{1}{|r|}{\textbf{AV}}& \textcolor{red}{0} & \sfrac{1}{3} & \sfrac{2}{3} & \textcolor{red}{1} & 1\sfrac{1}{3} & 1\sfrac{2}{3} & \textcolor{red}{2} & 2\sfrac{1}{3} & 2\sfrac{2}{3}\\
			\multicolumn{1}{|r|}{\textbf{F-number}} & \textcolor{red}{f/1} & f/1,1 & f/1,2 & \textcolor{red}{f/1,4} & f/1,6 & f/1,8 &	\textcolor{red}{f/2} & f/2,2 & f/2,5\\
			\hline

			\multicolumn{1}{|r|}{\textbf{AV}} & \textcolor{red}{3} & 3\sfrac{1}{3} & 3\sfrac{2}{3} & \textcolor{red}{4} & 4\sfrac{1}{3} & 4\sfrac{2}{3} & \textcolor{red}{5} & 5\sfrac{1}{3} & 5\sfrac{2}{3}\\
			\multicolumn{1}{|r|}{\textbf{F-number}} & \textcolor{red}{f/2,8} & f/3,2 & f/3,5 & \textcolor{red}{f/4} & f/4,5 & f/5 & \textcolor{red}{f/5,6} & f/6,3 & f/7,1\\
			\hline

			\multicolumn{1}{|r|}{\textbf{AV}} & \textcolor{red}{6} & 6\sfrac{1}{3} & 6\sfrac{2}{3} & \textcolor{red}{7} & 7\sfrac{1}{3} & 7\sfrac{2}{3} & \textcolor{red}{8} & 8\sfrac{1}{3} & 8\sfrac{2}{3}\\
			\multicolumn{1}{|r|}{\textbf{F-number}} & \textcolor{red}{f/8} & f/9 & f/10 & \textcolor{red}{f/11} & f/13 & f/14 & \textcolor{red}{f/16} & f/18 & f/20\\
			\toprule
		\end{tabular}
		\end{adjustbox}
	\end{table}
	
	Les ouvertures dont l'écart est de 1 stop sont marquées \textcolor{red}{en rouge}, coeff
	multiplicateur : $\sqrt{2} \approx 1,414$ (1,4 est admis).\\
	Modifier l'indice d'ouverture AV de 1 équivaut à :
	\begin{itemize}[label=\ding{43}]
		\item \ding{216} Fermer (augmenter $A_v$) $\Rightarrow -1 \text{ stop} \Rightarrow \acute{E}clairement\div2$
		\item \ding{218} Ouvrir (diminuer $A_v$) $\Rightarrow +1 \text{ stop} \Rightarrow \acute{E}clairement\times2$
	\end{itemize}
	
		\vspace*{-2cm}
	\subsection{Compensation en f(coeff multiplicateur)}\label{compensation-cm}
	\paragraph{Le rendu\\[12pt]}\label{le-rendu}
	Dans une \textbf{certaine mesure} (selon la lumière, les optiques et réglages disponibles, ainsi que les \og{}contraintes\fg{} de la scène) et dans des conditions équivalentes (distance, agrandissement\ldots), il est possible d'approcher le rendu que produirait un capteur plein-format, en adaptant la focale et les paramètres d'exposition en fonction du coefficient multiplicateur de son boîtier.
	
	On entend par rendu
	\begin{itemize}
		\item Le cadrage/champ de vision
		\item La profondeur de champ
		\item La quantité de bruit
	\end{itemize}	
	
	\vspace{0.2cm}
	Les coefficients multiplicateurs $CM$ usuels sont :
	
	\begin{description}
		\item[APS-H] $\textcolor{red}{\times1,3}$
		\item[APS-C Standard] $\textcolor{red}{\times1,5}$
		\item[APS-C Canon] $\textcolor{red}{\times1,6}$ (1,58 et des poussières)
		\item[Micro \sfrac{4}{3}] $\textcolor{red}{\times2}$ (entre 1,84 et 2 selon le rapport de forme)
	\end{description}
	
	\vspace{0.2cm}
	Exemple avec un boîtier APS-C
	$$Focale = \frac{Focale_{FullFrame}}{CM}$$
	
	\vspace{0.125cm}
	Même cadrage qu'un \textcolor{red}{\unit{85}{\milli\meter}}, il faut une focale $\textcolor{red}{\approx\unit{53}{\milli\meter}}$ sur un boîtier Canon, et $\textcolor{red}{\approx\unit{56-57}{\milli\meter}}$ pour tous les autres fabricants.
	
	$$Nombre f = \frac{Nombre f_{FullFrame}}{CM}$$
	
	\vspace{0.125cm}
	Même profondeur de champ qu'à \textcolor{red}{f/4}, il faut une ouverture \textcolor{red}{$\approx$ f/2.5}.
	
	$$ISO = \frac{ISO_{FullFrame}}{CM²}$$
	
	\vspace{0.125cm}
	Pour des capteurs de génération à peu près équivalente, et le même niveau de bruit qu'à \textcolor{red}{1600~ISO}, il faut \textcolor{red}{ISO~$\leqslant$~625} sur un boîtier Canon, et \textcolor{red}{ISO~$\leqslant$~711} pour tous les autres fabricants.
	
	\vspace{0.5cm}
	Note :\\
	\indent{}Si on veut agir sur le bruit, on doit pouvoir compenser avec une PdC plus faible, un temps de pose plus long et/ou en rajoutant de l'éclairage.

\end{document}